package plugin

import "testing"

func TestMatchFilename(t *testing.T) {
	tests := []struct {
		filename string
		want     bool
	}{
		{
			filename: "hello.c",
			want:     true,
		},
		{
			filename: "no.txt",
			want:     false,
		},
		{
			filename: "I-love.c++",
			want:     true,
		},
		{
			filename: "only.h",
			want:     true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.filename, func(t *testing.T) {
			if got := MatchFilename(tt.filename); got != tt.want {
				t.Errorf("MatchFilename() = %v, want %v", got, tt.want)
			}
		})
	}
}
