package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

const (
	columnFile = iota
	columnLine
	columnColumn
	columnDefaultLevel
	columnLevel
	columnCategory
	columnName
	columnWarning
	columnSuggestion
	columnNote
	columnCWEs
	columnContext
	columnFingerprint
	columnToolVersion
	columnRuleID
	columnHelpURI
)

func convert(r io.Reader, prependPath string) (*report.Report, error) {
	reader := csv.NewReader(r)
	records, err := reader.ReadAll()
	if err != nil {
		return nil, err
	}

	newReport := report.NewReport()

	if len(records) < 2 {
		return &newReport, nil // empty document
	}
	records = records[1:]

	vulns := make([]report.Vulnerability, len(records))
	for i, sliceOfStrings := range records {
		record := Record(sliceOfStrings)
		r := Result{record, prependPath}
		vulns[i] = report.Vulnerability{
			Category:    report.CategorySast,
			Scanner:     &metadata.IssueScanner,
			Message:     r.Warning(),
			CompareKey:  r.CompareKey(),
			Severity:    r.Severity(),
			Location:    r.Location(),
			Identifiers: r.Identifiers(),
			Solution:    r.Suggestion(),
		}
	}

	newReport.Vulnerabilities = vulns
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Version = report.Version{
		Major: 15,
		Minor: 0,
		Patch: 7,
	}
	return &newReport, nil
}

// Result describes a result with a relative path.
type Result struct {
	Record
	PrependPath string
}

// Filepath returns the relative path of the affected file.
func (r Result) Filepath() string {
	return filepath.Join(r.PrependPath, r.File())
}

// CompareKey returns a string used to establish whether two issues are the same.
func (r Result) CompareKey() string {
	fingerprint, cwes := r.Record[columnFingerprint], r.Record[columnCWEs]
	return strings.Join([]string{r.Filepath(), fingerprint, cwes}, ":")
}

// Record wraps a line of Flawfinder's output.
type Record []string

// File returns the path of the affected file.
func (r Record) File() string {
	return r[columnFile]
}

// Line returns the line number of the affected code.
func (r Record) Line() int {
	line, _ := strconv.Atoi(r[columnLine])
	return line
}

// Name returns the function name (id) of the affected code.
func (r Record) Name() string {
	return r[columnName]
}

// CWEs return a list of CWE IDs based on the columnCWEs which supports multiple formats:
// "CWE-119" => the vulnerability is mapped to CWE-119.
// "CWE-119, CWE-120" => the vulnerability is mapped to both CWE-119 and CWE-120.
// "CWE-119!/CWE-120" => the vulnerability is mapped to CWE-119 and CWE-120 is a subset of CWE-119.
// "CWE-362/CWE-367!" => the vulnerability is mapped to CWE-367 and CWE-367 is a subset of CWE-362.
func (r Record) CWEs() []int {
	rawCWEs := strings.FieldsFunc(r[columnCWEs], func(r rune) bool {
		return r == ',' || r == '/'
	})
	cwes := make([]int, len(rawCWEs))
	for i, cwe := range rawCWEs {
		// Remove bang, space and 'CWE-' prefix to have ID only
		id, err := strconv.Atoi(strings.Trim(cwe, "! CWE-"))
		if err != nil {
			continue
		}
		cwes[i] = id
	}

	return cwes
}

// Warning returns a warning message that explains the vulnerability.
func (r Record) Warning() string {
	return r[columnWarning]
}

// Suggestion returns a message on how to fix the vulnerability.
func (r Record) Suggestion() string {
	return r[columnSuggestion]
}

// Location returns a structured Location
func (r Result) Location() report.Location {
	return report.Location{
		File:      r.Filepath(),
		LineStart: r.Line(),
	}
}

// Severity returns the normalized severity
// See https://sourceforge.net/p/flawfinder/code/ci/2.0.5/tree/flawfinder
func (r Record) Severity() report.SeverityLevel {
	level, err := strconv.Atoi(r[columnLevel])
	if err != nil {
		return report.SeverityLevelUnknown
	}

	// "risk_level" goes from 0 to 5. 0=no risk, 5=maximum risk
	switch level {
	case 5:
		return report.SeverityLevelCritical
	case 4:
		return report.SeverityLevelHigh
	case 3:
		return report.SeverityLevelMedium
	case 1, 2:
		return report.SeverityLevelLow
	case 0:
		return report.SeverityLevelInfo
	}
	return report.SeverityLevelUnknown
}

// Identifiers returns the normalized identifiers of the vulnerability.
func (r Record) Identifiers() []report.Identifier {
	identifiers := []report.Identifier{
		r.FlawfinderIdentifier(),
	}

	cwes := r.CWEs()
	for i := range cwes {
		cwe := cwes[i]
		identifiers = append(identifiers, report.CWEIdentifier(cwe))
	}

	return identifiers
}

// FlawfinderIdentifier returns a structured Identifier for a Flawfinder Name
func (r Record) FlawfinderIdentifier() report.Identifier {
	return report.Identifier{
		Type:  "flawfinder_func_name",
		Name:  fmt.Sprintf("Flawfinder - %s", r.Name()),
		Value: r.Name(),
	}
}
