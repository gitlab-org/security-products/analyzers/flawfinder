package main

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestListIncludedPaths(t *testing.T) {
	excludedGlobs := []string{"**/bad", "**/exclude-me.c", "not-me"}
	paths, err := listIncludedPaths("test/excluded", excludedGlobs)
	require.NoError(t, err)
	require.Equal(t, strings.Fields("a b/hello.c c/c1/hello.c c/utils.c"), paths)

	excludedGlobs = []string{"not-me"}
	paths, err = listIncludedPaths("test/excluded", excludedGlobs)
	require.NoError(t, err)
	require.Equal(t, strings.Fields("a b c"), paths)

	excludedGlobs = []string{"not-existed"}
	paths, err = listIncludedPaths("test/excluded", excludedGlobs)
	require.NoError(t, err)
	require.Equal(t, strings.Fields("."), paths)

	excludedGlobs = []string{}
	paths, err = listIncludedPaths("test/excluded", excludedGlobs)
	require.NoError(t, err)
	require.Equal(t, strings.Fields("."), paths)
}
