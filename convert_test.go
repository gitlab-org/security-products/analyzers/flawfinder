package main

import (
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/flawfinder/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v4"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2"
)

func TestConvert(t *testing.T) {
	in := `File,Line,Column,DefaultLevel,Level,Category,Name,Warning,Suggestion,Note,CWEs,Context,Fingerprint,ToolVersion,RuleId,HelpUri
./sessionmanager.cpp,10,6,1,3,random,srand,This function is not sufficiently random for security-related functions such as key and nonce creation (CWE-327),Use a more secure technique for acquiring random values,,CWE-327,srand(QDateTime::currentDateTime().toTime_t());,a3e2b870a89ea8ce24feaff1c0be2b3abae3c7ddb04473e3bb36aef0cd2a0692,2.0.17,FF0000,https://cwe.mitre.org/data/definitions/327.html
./http_parser.c,110,14,1,2,buffer,char,"Statically-sized arrays can be improperly restricted, leading to potential overflows or other issues (CWE-119!/CWE-120)","Perform bounds checking, use functions that limit length, or ensure that the size is larger than the maximum possible length",,CWE-119!/CWE-120,static const char tokens[256] = {,854dafbb47a004584e5c8f9fe12485c59819f1285f0dc47335495199109e1d58,2.0.17,FF0000,https://cwe.mitre.org/data/definitions/327.html
./pagetemplate.cpp,57,14,1,2,misc,open,"Check when opening files - can an attacker redirect it (via symlinks), force the opening of special file type (e.g., device files), move things around to create a race condition, control its ancestors, or change its contents? (CWE-362)",,,CWE-362,    if (data.open(QFile::ReadOnly )),55c652bbf87ea791152438117629f6cb843e4ba3a51ce7eae2f2316ed25fcdac,2.0.17,FF0000,https://cwe.mitre.org/data/definitions/327.html
./sessionmanager.cpp,139,5,1,2,buffer,char,"Statically-sized arrays can be improperly restricted, leading to potential overflows or other issues (CWE-119!/CWE-120)","Perform bounds checking, use functions that limit length, or ensure that the size is larger than the maximum possible length",,CWE-119!/CWE-120,    char sessionKey[65];,eb75fefb56d710020ca611d20ff390370a7ac03fe822ebab10f22cf43510e683,2.0.17,FF0000,https://cwe.mitre.org/data/definitions/327.html
./worker.cpp,205,22,1,2,misc,fopen,"Check when opening files - can an attacker redirect it (via symlinks), force the opening of special file type (e.g., device files), move things around to create a race condition, control its ancestors, or change its contents? (CWE-362)",,,CWE-362,"                file=fopen((QString(""file/"")+socket->getHeader().getPath().right(socket->getHeader().getPath().count()-6)).toStdString().c_str(),""rb"");",105c759f145d1ddc78932ed42249ae3a494856aec55ae7dc9f0640fff4e59d26,2.0.17,FF0000,https://cwe.mitre.org/data/definitions/327.html`

	var scanner = &metadata.IssueScanner

	version := report.Version{
		Major: 15,
		Minor: 0,
		Patch: 7,
	}
	want := &report.Report{
		Version: version,
		Vulnerabilities: []report.Vulnerability{
			{
				Category:   report.CategorySast,
				Scanner:    scanner,
				Message:    "This function is not sufficiently random for security-related functions such as key and nonce creation (CWE-327)",
				CompareKey: "app/sessionmanager.cpp:a3e2b870a89ea8ce24feaff1c0be2b3abae3c7ddb04473e3bb36aef0cd2a0692:CWE-327",
				Location: report.Location{
					File:      "app/sessionmanager.cpp",
					LineStart: 10,
				},
				Severity: report.SeverityLevelMedium,
				Solution: "Use a more secure technique for acquiring random values",
				Identifiers: []report.Identifier{
					{
						Type:  "flawfinder_func_name",
						Name:  "Flawfinder - srand",
						Value: "srand",
					},
					{
						Type:  "cwe",
						Name:  "CWE-327",
						Value: "327",
						URL:   "https://cwe.mitre.org/data/definitions/327.html",
					},
				},
			},
			{
				Category:   report.CategorySast,
				Scanner:    scanner,
				Message:    "Statically-sized arrays can be improperly restricted, leading to potential overflows or other issues (CWE-119!/CWE-120)",
				CompareKey: "app/http_parser.c:854dafbb47a004584e5c8f9fe12485c59819f1285f0dc47335495199109e1d58:CWE-119!/CWE-120",
				Location: report.Location{
					File:      "app/http_parser.c",
					LineStart: 110,
				},
				Severity: report.SeverityLevelLow,
				Solution: "Perform bounds checking, use functions that limit length, or ensure that the size is larger than the maximum possible length",
				Identifiers: []report.Identifier{
					{
						Type:  "flawfinder_func_name",
						Name:  "Flawfinder - char",
						Value: "char",
					},
					{
						Type:  "cwe",
						Name:  "CWE-119",
						Value: "119",
						URL:   "https://cwe.mitre.org/data/definitions/119.html",
					},
					{
						Type:  "cwe",
						Name:  "CWE-120",
						Value: "120",
						URL:   "https://cwe.mitre.org/data/definitions/120.html",
					},
				},
			},
			{
				Category:   report.CategorySast,
				Scanner:    scanner,
				Message:    "Check when opening files - can an attacker redirect it (via symlinks), force the opening of special file type (e.g., device files), move things around to create a race condition, control its ancestors, or change its contents? (CWE-362)",
				CompareKey: "app/pagetemplate.cpp:55c652bbf87ea791152438117629f6cb843e4ba3a51ce7eae2f2316ed25fcdac:CWE-362",
				Location: report.Location{
					File:      "app/pagetemplate.cpp",
					LineStart: 57,
				},
				Severity: report.SeverityLevelLow,
				Identifiers: []report.Identifier{
					{
						Type:  "flawfinder_func_name",
						Name:  "Flawfinder - open",
						Value: "open",
					},
					{
						Type:  "cwe",
						Name:  "CWE-362",
						Value: "362",
						URL:   "https://cwe.mitre.org/data/definitions/362.html",
					},
				},
			},
			{
				Category:   report.CategorySast,
				Scanner:    scanner,
				Message:    "Statically-sized arrays can be improperly restricted, leading to potential overflows or other issues (CWE-119!/CWE-120)",
				CompareKey: "app/sessionmanager.cpp:eb75fefb56d710020ca611d20ff390370a7ac03fe822ebab10f22cf43510e683:CWE-119!/CWE-120",
				Location: report.Location{
					File:      "app/sessionmanager.cpp",
					LineStart: 139,
				},
				Severity: report.SeverityLevelLow,
				Solution: "Perform bounds checking, use functions that limit length, or ensure that the size is larger than the maximum possible length",
				Identifiers: []report.Identifier{
					{
						Type:  "flawfinder_func_name",
						Name:  "Flawfinder - char",
						Value: "char",
					},
					{
						Type:  "cwe",
						Name:  "CWE-119",
						Value: "119",
						URL:   "https://cwe.mitre.org/data/definitions/119.html",
					},
					{
						Type:  "cwe",
						Name:  "CWE-120",
						Value: "120",
						URL:   "https://cwe.mitre.org/data/definitions/120.html",
					},
				},
			},
			{
				Category:   report.CategorySast,
				Scanner:    scanner,
				Message:    "Check when opening files - can an attacker redirect it (via symlinks), force the opening of special file type (e.g., device files), move things around to create a race condition, control its ancestors, or change its contents? (CWE-362)",
				CompareKey: "app/worker.cpp:105c759f145d1ddc78932ed42249ae3a494856aec55ae7dc9f0640fff4e59d26:CWE-362",
				Location: report.Location{
					File:      "app/worker.cpp",
					LineStart: 205,
				},
				Severity: report.SeverityLevelLow,
				Identifiers: []report.Identifier{
					{
						Type:  "flawfinder_func_name",
						Name:  "Flawfinder - fopen",
						Value: "fopen",
					},
					{
						Type:  "cwe",
						Name:  "CWE-362",
						Value: "362",
						URL:   "https://cwe.mitre.org/data/definitions/362.html",
					},
				},
			},
		},
		Analyzer: "flawfinder",
		Config:   ruleset.Config{Path: ruleset.PathSAST},
	}
	r := strings.NewReader(in)
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
